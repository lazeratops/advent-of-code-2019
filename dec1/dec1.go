package main

import (
	"aoc2019/util"
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"runtime"
	"strconv"
)

/*
Day 1, parts 1 + 2

Fuel required to launch a given module is based on its mass.
Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.
Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and subtract 2.
However, that fuel also requires fuel, and that fuel requires fuel, and so on.
Any mass that would require negative fuel should instead be treated as if it requires zero fuel;
the remaining mass, if any, is instead handled by wishing really hard,
which has no mass and is outside the scope of this calculation.
*/

func main() {
	fuel, err := runPartOne(nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Part 1: %d", fuel)

	fuel, err = runPartTwo(nil)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("\nPart 2: %d", fuel)

}

func runPartOne(input io.Reader) (int, error) {
	return run(calcFuel, input)
}

func runPartTwo(input io.Reader) (int, error) {
	return run(calcRealFuel, input)
}

// What is the sum of the fuel requirements for all of the modules on your spacecraft?
func run(calcFunc func(int) int, input io.Reader) (int, error) {
	var scanner *bufio.Scanner
	if input == nil {
		inputFile := getInputFile()
		defer inputFile.Close()
		input = inputFile
	}
	scanner = bufio.NewScanner(input)

	var totalFuel int
	for scanner.Scan() {
		line := scanner.Text()
		moduleMass, err := strconv.Atoi(line)
		if err != nil {
			return -1, err
		}
		fuel := calcFunc(moduleMass)
		totalFuel += fuel
	}

	return totalFuel, nil
}

func calcFuel(mass int) int {
	return int(math.Floor(float64(mass/3)) - 2)
}

func calcRealFuel(mass int) int {
	fuel := calcFuel(mass)
	if fuel < 0 {
		return 0
	}
	return fuel + calcRealFuel(fuel)
}

func getInputFile() *os.File {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil
	}
	return util.GetInputFile(filename)
}
